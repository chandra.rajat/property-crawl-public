# property-crawl-public

## Usage

- Replace constants in `main.py`
- Get twilio account, auth-token & number. See example here https://www.twilio.com/docs/sms/quickstart/python
- Replace the receiving number with your number
- Get search urls from zoopla/right-move and replace in dictionary  
- Setup cron on local machine
