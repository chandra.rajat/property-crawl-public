from urllib import request
import json
from bs4 import BeautifulSoup
from twilio.rest import Client

MIN_PRICE = INT_MIN
MAX_PRICE = INT_MAX

MIN_BED = INT_BED
DELIMITER = "#"

NUMBERS = ["+447556006141"] # Replace number

OUTPUT_FILE = "/home/rajat/code/property-crawl/property-data" # Path to local data file

ZOOPLA = [
    {
        "zo_sm2_address": (
            f"https://www.zoopla.co.uk/for-sale/houses/sm2/"
            f"?beds_min={MIN_BED}&page_size=25&price_max={MAX_PRICE}&price_min={MIN_PRICE}&q=SM2"
            f"&radius=0.5&results_sort=newest_listings&search_source=refine"
        )
    },
    {
        "zo_purley_address": (
            f"https://www.zoopla.co.uk/for-sale/houses/"
            f"london/purley/?&beds_min={MIN_BED}&price_max={MAX_PRICE}&"
            f"price_min={MIN_PRICE}&q=Purley%2C%20London&results_sort=newest_listings&search_source=home"
        )
    },
    {
        "zo_wallington_address": (
            f"https://www.zoopla.co.uk/for-sale/houses/"
            f"london/wallington/?beds_min={MIN_BED}&page_size=25&price_max={MAX_PRICE}&"
            f"price_min={MIN_PRICE}&q=Wallington%2C%20London&radius=0&results_sort=newest_listings&search_source=facets"
        )
    },
]

RIGHTMOVE = [
    {
        "rm_purley_address": (
            f"https://www.rightmove.co.uk/property-for-sale/find.html?"
            f"locationIdentifier=REGION%5E1105&minBedrooms={MIN_BED}&"
            f"maxPrice={MAX_PRICE}&minPrice={MIN_PRICE}&propertyTypes=&mustHave=&dontShow=&furnishTypes=&keywords="
        )
    },
    {
        "rm_sm2_address": (
            f"https://www.rightmove.co.uk/property-for-sale/"
            f"find.html?locationIdentifier=REGION%5E1297&minBedrooms={MIN_BED}&"
            f"maxPrice={MAX_PRICE}&minPrice={MIN_PRICE}&propertyTypes=&mustHave=&dontShow=&furnishTypes=&keywords="
        )
    },
    {
        "rm_wallington_address": (
            f"https://www.rightmove.co.uk/property-for-sale/find.html?"
            f"searchType=SALE&locationIdentifier=REGION%5E1390&insId=1&radius=0.0&"
            f"minPrice={MIN_PRICE}&maxPrice={MAX_PRICE}&minBedrooms={MIN_BED}"
            f"&maxBedrooms=&displayPropertyType=houses&maxDaysSinceAdded=&_includeSSTC=on"
            f"&sortByPriceDescending=&primaryDisplayPropertyType=&secondaryDisplayPropertyType="
            f"&oldDisplayPropertyType=&oldPrimaryDisplayPropertyType=&newHome=&auction=false"
        )
    },
]


def send_text(numbers, body):
    client = Client(
        "twilio-account-sid", "twilio-auth-token"
    )
    for number in numbers:
        client.messages.create(to=number, from_="twilio-number", body=body)


def load_existing_records():
    records = []
    count = 0
    try:
        with open(OUTPUT_FILE, "r") as infile:
            for pid in infile:
                print(pid)
                records.append(int(pid))
                count += 1
            print(f"total lines read {count}")
    except Exception as e:
        print(e)

    return set(records)


def write_records(records):
    count = 0
    with open(OUTPUT_FILE, "w") as output:
        for pid in records:
            output.write(
                f"{pid}\n"
            )
            count += 1
        print(f"total lines written {count}")


def run_zo_query(query, records):
    print(query)
    req = request.Request(query, headers={"User-Agent": "Mozilla"})
    with request.urlopen(req) as url:
        data = url.read()
        soup = BeautifulSoup(data, "html.parser")

        for tag in soup.find_all("script"):
            if "id" in tag.attrs and tag["id"] == "__NEXT_DATA__":
                properties = json.loads(tag.string[0:])["props"]["initialProps"][
                    "pageProps"
                ]["regularListingsFormatted"]

                print(len(properties))
                for prop in properties:
                    property_id = int(prop["listingId"])
                    records[property_id] = (
                        "Z",
                        property_id,
                        prop["price"][1:],
                        prop["address"][0:25],
                    )


def run_rm_query(query, records):
    print(query)
    req = request.Request(query, headers={"User-Agent": "Mozilla"})
    with request.urlopen(req) as url:
        data = url.read()
        soup = BeautifulSoup(data, "html.parser")

        for tag in soup.find_all("script"):
            if tag.string and tag.string.startswith("window.jsonModel"):
                start_index = tag.string.index("{")
                properties = json.loads(tag.string[start_index:])["properties"]

                print(len(properties))
                for prop in properties:
                    property_id = int(prop["id"])
                    records[property_id] = (
                        "R",
                        property_id,
                        prop["price"]["amount"],
                        prop["displayAddress"][0:25],
                    )


if __name__ == "__main__":
    new_records = {}
    for address in RIGHTMOVE:
        run_rm_query(next(iter(address.values())), new_records)

    for address in ZOOPLA:
        run_zo_query(next(iter(address.values())), new_records)

    old_property_ids = load_existing_records()
    print(f"old property ids {len(old_property_ids)} :  {old_property_ids}")

    new_property_ids = {k for k in new_records.keys()} if new_records else set()
    print(f"new property ids {len(new_property_ids)} :  {new_property_ids}")

    print(f"records in old that are not in new {old_property_ids - new_property_ids}")

    for pid in new_property_ids - old_property_ids:
        print(f"Found new pid {pid}")
        send_text(NUMBERS, f"New Property found: {new_records[pid]}")

    write_records(new_property_ids | old_property_ids)
